//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
// si on aura au préalable défini 1 item, il faudra, pour les accumuler, créer un tableau, vide au départ :
const items = ["Site vitrine de La Ferme des itinérants","Module de commande de produits", "Moteur d'arguments intellectuellement malhonnêtes", "Faire son fromage de chèvre pas à pas", "Routine d'étirements, renforcement musculaire et proprioception", "Trouver du (bon) champagne (pas cher) au verre à Bordeaux"];

app.set("view engine", "ejs");

// pour utiliser l'élément qui serait entré dans le corps d'un autre fichier :
app.use(bodyParser.urlencoded({extended: true}));

// indiquer l'emplacement des fichiers statiques :
app.use(express.static("public"));

// affichage de de cette route à l'accueil :
app.get("/", function(req, res){
  let today = new Date();
  let options = {
    weekday : "long",
    day: "numeric",
    month: "long"
  }
  let day = today.toLocaleDateString(undefined, options);

  // EXERCICE : mettre la première lettre du jour en majuscule
  // https://stackoverflow.com/questions/58531156/javascript-how-to-format-the-date-according-to-the-french-convention
  // let weekdayCapital = first.toUpperCase() + rest.join('').toLowerCase();

  // prendre la valeur de la variable day et l'attribuer, dans le fichier "list", à l'objet kindOfDay
  res.render("list", {
    kindOfDay: day,
    // on ajoute ici le nouveau projet de la liste ou carrément tout le tableau d'entrée :
    newListItems: items
  });
  // ejs nous permet d'utiliser res.render pour échanger des données entre fichiers, donc la variable day, pour qu'elle équivale à l'objet kindOfDay du fichier list

});
// route d'accueil qui rendra 2 variables : kindOfDay et newListItems
// newListItems étant définie pour être égale au tableau items commençant par mes éléments pré-listés
// les 2 variables sont donc envoyées au fichier "list" contenant lui-même des fonctions

// une fois l'app.use bodyParser activée, on peut aller chercher la donnée entrée (input) :
app.post("/", function(req, res){
  // on enregistre la donnée newItem envoyée (postée) via le formulaire du fichier "list"
  let item = req.body.newItem; 
  // on ajoute au tableau/array le nouvel item entré/input :
  items.push(item);
  // ici, au lieu de réutiliser res.render("list", {newListItem: item}) on ajoute juste au-dessus l'objet à renvoyer
  res.redirect("/");
  // et on renvoie à la route initiale / accueil
});

app.listen(3000, function(){
  console.log("Server started on port 3000.");
});

// affichage-test de départ : res.send("Hello");