app.get("/", function(req, res){
    const today = new Date();

    // pour raccourcir le code on nommera la constante au préalable, via le code natif get.day de javascript
    const currentDay = today.getDay();
      // à ne pas confondre avec getDate qui donne la date (17 pour lundi 17 juillet par ex.)


    // test pour savoir si aujourd'hui est en semaine ou en week-end (samedi égal 6 et dimanche 0) 
    if (currentDay === 6 || currentDay === 0) {
        // on utilisera res.write pour envoyer plusieurs données quand res.send nous limite à 1 seul élément
        res.write("Week-end meuf!");
    } else {
        res.write("Busy(ness) day...");
        // avant d'envoyer le tout
        res.send();
    }
});