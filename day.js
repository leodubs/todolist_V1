//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.set("view-engine", "ejs");

app.get("/", function(req, res){
    const today = new Date();

    // pour raccourcir le code on nommera la constante au préalable, via le code natif get.day de javascript
    const currentDay = today.getDay();
  // à ne pas confondre avec getDate qui donne la date (17 pour lundi 17 juillet par ex.)

    // création d'une variable day pour voir, plus tard, si elle correspond à un jour de semaine ou week-end
    let day = "";


    // switch statement
    switch (currentDay){
        case 0:
            day = "Dimanche";
            break;
        case 1:
            day = "Lundi";
            break;
        case 2:
            day = "Mardi";
            break;
        case 3:
            day = "Mercredi";
            break;
        case 4:
            day = "Jeudi";
            break;
        case 5:
            day = "Vendredi";
            break;
        case 6:
            day = "Samedi";
            break;
        default:
            console.log("Error: current day is equal to: " + currentDay)
    }
});

app.listen(3000, function(){
    console.log("Server started on port 3000");
});